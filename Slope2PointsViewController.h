//
//  Slope2PointsViewController.h
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"
#import <iAd/iAd.h>



#define kKeyboardAnimationDuration .3
#define kTabBarHeight 50
@class DetailViewController;

@interface Slope2PointsViewController : UIViewController <UITextFieldDelegate, ADBannerViewDelegate, UISplitViewControllerDelegate> 
{
    ADBannerView *adView;
    BOOL bannerIsVisible;
    BOOL keyboardVisible;
    UITextField *activeField;
    double x1;
    double y1;
    double x2;
    double y2;
    double slope;

}
@property (nonatomic, strong) NSMutableArray *modelArray;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField *x1TextField;
@property (nonatomic, retain) IBOutlet UITextField *y1TextField;
@property (nonatomic, retain) IBOutlet UITextField *x2TextField;
@property (nonatomic, retain) IBOutlet UITextField *y2TextField;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet ADBannerView *adView;
@property (nonatomic, assign) BOOL bannerIsVisible;

-(IBAction)calculateButtonPressed:(id)sender;
- (void) createContentPages;
-(IBAction)clearButtonPressed:(id)sender;


@end
