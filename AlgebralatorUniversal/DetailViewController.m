//
//  DetailViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "ContentViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

@synthesize detailItem = _detailItem;
@synthesize masterPopoverController = _masterPopoverController;
@synthesize adView,dataObject, webViewString, webView, paperView ;
#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
       // [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}
-(void)clearWebView{
    [appDelegate.summaryStringsArray removeAllObjects];
    [self viewWillAppear:YES];
}
- (void)configureView
{
   
    

    for(int i = 0; i < [appDelegate.summaryStringsArray count] ; i++){
        [webViewString appendString:[appDelegate.summaryStringsArray objectAtIndex:i]];
        
    }
    [self.webView loadHTMLString:webViewString baseURL:nil];
    //[webViewString setString:@""];
    

}
#pragma - mark
#pragma iad Banner


-(void) moveBannerOffscreen
{
    /*CGRect originalScrollFrame = self.scrollView.frame   ;
    CGFloat newScrollHeight = self.view.frame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
    newScrollFrame.size.height = newScrollHeight;*/
    
    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = -70;
    
   // self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    
}
-(void) moveBannerOnScreen
{
        
        CGRect newBannerFrame = self.adView.frame;
        newBannerFrame.origin.y = 0;
   
      /*  CGRect originalScrollFrame = self.scrollView.frame   ;
        //CGFloat newScrollHeight = self.view.frame.size.height - newBannerFrame.size.height;
        CGRect newScrollFrame = originalScrollFrame;
        //newScrollFrame.size.height = newScrollHeight;*/
        
        [UIView beginAnimations:@"BannerViewIntro" context:NULL];
        //self.scrollView.frame = newScrollFrame;
        self.adView.frame = newBannerFrame;
        [UIView commitAnimations];
    }



-(void) viewWillAppear:(BOOL)animated{
    //[self moveBannerOffscreen];
   // CGRect newScrollFrame = self.scrollView.frame;
   // newScrollFrame.origin.y = 70;
   // if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self.webViewString setString:@""];
        [self configureView];
   // }
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    }else if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

        self. adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
        }
        //self.adView.frame = CGRectMake(0, 0, 1004, 70);
       

    }
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self moveBannerOffscreen];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self moveBannerOnScreen];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
        //self.adView.frame = CGRectMake(0, 0, 1004, 70);
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

        self.adView.currentContentSizeIdentifier =ADBannerContentSizeIdentifierLandscape;
        }
    } else{
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierPortrait;
    }
   
}

-(BOOL) allowActionToRun{
    return YES;
    
}
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = [self allowActionToRun]; // your application implements this method
    if (!willLeave && shouldExecuteAction)
    {
        // insert code here to suspend any services that might conflict with the advertisement
    }
    return shouldExecuteAction;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self moveBannerOffscreen];
    
   // CGRect newScrollFrame = self.scrollView.frame;
   // newScrollFrame.origin.y = 70; 
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc] 
                                      initWithTitle:@"Clear"                                            
                                      style:UIBarButtonItemStyleBordered 
                                      target:self 
                                      action:@selector(clearWebView)];
    self.navigationItem.rightBarButtonItem = clearButton;    
   
    webViewString = [[NSMutableString alloc] initWithString: @""];
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    

    [[webView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[webView layer] setBorderWidth:2.3];
    [[webView layer] setCornerRadius:15];
    [webView setClipsToBounds: YES];
    
}
    

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
   // self.webViewString = nil;
   // self.solutionCount = 0;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}



- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
    //self.webViewString = nil;
    //self.solutionCount = 0;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
            return YES;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Algebralator", @"Algebralator");
    }
    return self;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Functions", @"Functions");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}


@end
