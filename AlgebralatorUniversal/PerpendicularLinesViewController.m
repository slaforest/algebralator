//
//  PerpendicularLinesViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 1/8/12.
//  Copyright (c) 2012 Scott LaForest. All rights reserved.
//

#import "PerpendicularLinesViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"

@implementation PerpendicularLinesViewController
@synthesize instructionsText, answerTextView, directionsTextView, mTextField, bTextField, xTextField, yTextField, m, b, x, y, lineType, variable1Label, variable2Label, navigationBar,scrollView, bannerIsVisible, adView, mainDetailViewController, summaryViewController, detailViewController;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2*activeField.frame.origin.y);//-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}
#pragma - mark
#pragma iad Banner


-(void) moveBannerOffscreen
{
    CGRect originalScrollFrame = self.scrollView.frame   ;
    CGFloat newScrollHeight = self.view.frame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
    newScrollFrame.size.height = newScrollHeight;
    
    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = -75;
    
    self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    
}
-(void) moveBannerOnScreen
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = 0;
    
    CGRect originalScrollFrame = self.scrollView.frame   ;
    //CGFloat newScrollHeight = self.view.frame.size.height - newBannerFrame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
   // newScrollFrame.size.height = newScrollHeight;
    
    [UIView beginAnimations:@"BannerViewIntro" context:NULL];
    self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    [UIView commitAnimations];
    }
}
-(void) viewWillAppear:(BOOL)animated{
    //[self moveBannerOffscreen];
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    }else{
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.adView = nil;
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self moveBannerOffscreen];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self moveBannerOnScreen];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierLandscape;
    else
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierPortrait;
}

-(BOOL) allowActionToRun{
    return YES;
    
}
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = [self allowActionToRun]; // your application implements this method
    if (!willLeave && shouldExecuteAction)
    {
        // insert code here to suspend any services that might conflict with the advertisement
    }
    return shouldExecuteAction;
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    /*if (textField == mTextField){
        [bTextField becomeFirstResponder];
    }else if(textField == bTextField){
        [xTextField becomeFirstResponder];
    }else if(textField == xTextField){
        [yTextField becomeFirstResponder];
    }*/

    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
	[sender resignFirstResponder];	
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar c = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];

        return NO;
    }
    
    
    return YES;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    [self moveBannerOffscreen];
    [self registerForKeyboardNotifications];

    self.adView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait,ADBannerContentSizeIdentifierLandscape,nil];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    self.mTextField.delegate = self;
    self.bTextField.delegate = self;
    self.xTextField.delegate = self;
    self.yTextField.delegate = self;
    
    
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]); 
    
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
}

 

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}
-(IBAction)clearButtonPressed:(id)sender{
    
    mTextField.text = @"";
    bTextField.text = @"";
    xTextField.text = @"";
    yTextField.text = @"";
    answerTextView.text = @"";
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

-(IBAction) calculateButtonPressed: (id) sender{
	self.m = [mTextField.text doubleValue];
	self.b = [bTextField.text doubleValue];
	self.x = [xTextField.text doubleValue];
	self.y = [yTextField.text doubleValue];
	
	
    if(lineType.selectedSegmentIndex == 0){
        answerTextView.text = [NSString stringWithFormat:@"x = %g  ", x];
    }
    else if(lineType.selectedSegmentIndex == 1){
        answerTextView.text = [NSString stringWithFormat:@"y = %g  ", y];
    }else{
        newM = (-1/m);
        newB = y - newM*x;
	
            if (newB == 0) {
                answerTextView.text = [NSString stringWithFormat:@"y = %g x ", newM];
            }else if(newM == 0){//same as selectedSegmentIndex == 0
                answerTextView.text = [NSString stringWithFormat:@"y = %g", newB];	
            }else {
                answerTextView.text = [NSString stringWithFormat:@"y = %g x + %g",newM, newB];	
            }
    }
    [self createContentPages];

}

-(IBAction) segmentedControlIndexChanged{
    switch (self.lineType.selectedSegmentIndex) {
            
        case 0://set UI for horizontal lines
            self.variable1Label.text =@"y =";
            self.variable2Label.text =@" ";
            self.mTextField.placeholder = @"";
            self.bTextField.text = @"";
            self.bTextField.hidden = YES;
            [self.view setNeedsDisplay];
            break;
            
        case 1://set UI for vertical lines
            self.variable1Label.text =@"x =";
            self.variable2Label.text =@" ";
            self.mTextField.placeholder = @"";
            self.bTextField.text = @"";
            self.bTextField.hidden = YES;
            [self.view setNeedsDisplay];
            
            break;
            
        case 2:
            self.variable1Label.text =@"y =";
            self.variable2Label.text =@"x +";
            self.bTextField.hidden = NO;
            [self.view setNeedsDisplay];
            
            break;
            
            
    }
    
}
- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSString *contentString;
    if (self.lineType.selectedSegmentIndex == 0) {
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br />Line parallel to y = %g  and passing through (%g, %g) <br />%@ </h2></body></html>",dateString, m, x, y,answerTextView.text];
    }else if (self.lineType.selectedSegmentIndex == 1){
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br />Line parallel to x =  %g and passing through (%g, %g) <br />%@ </h2></body></html>",dateString, m, x, y,answerTextView.text];
    }else{
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br />Line parallel to y = %gx + %g and passing through (%g, %g) <br />%@ </h2></body></html>",dateString, m, b, x, y,answerTextView.text];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        
[mainDetailViewController viewWillAppear:YES];        
        
    }
}

- (void)dealloc {
    //[super dealloc];
}


@end
