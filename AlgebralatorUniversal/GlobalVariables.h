//
//  GlobalVariables.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVariables : NSObject
{
    NSMutableString *webViewString ;
}
@property (nonatomic,retain) NSMutableString *webViewString;

+ (GlobalVariables *)sharedInstance;
@end
