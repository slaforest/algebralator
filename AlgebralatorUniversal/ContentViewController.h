//
//  ContentViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *contentWebview;
@property (strong, nonatomic) NSMutableString *contentWebViewString;
@property (strong, nonatomic) IBOutlet UILabel *pageCountLabel;
@property(readwrite, nonatomic) int totalPages;
@property(readwrite, nonatomic) int currentPage;





@end
