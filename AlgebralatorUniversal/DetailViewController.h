//
//  DetailViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "AppDelegate.h"
@interface DetailViewController : UIViewController <UISplitViewControllerDelegate/*, UIPageViewControllerDelegate,UIPageViewControllerDataSource*/>
{
    AppDelegate *appDelegate;
}
//@property (nonatomic, retain) UIPageViewController *pageViewController;
//@property (nonatomic, strong) NSMutableArray *modelArray;
@property (strong, nonatomic) id detailItem;
@property (strong, nonatomic) IBOutlet ADBannerView *adView;
@property (strong, nonatomic) NSMutableString *webViewString;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIImageView *paperView;



@property (strong, nonatomic) id dataObject;
-(void)clearWebView;
-(void)configureView;


@end
