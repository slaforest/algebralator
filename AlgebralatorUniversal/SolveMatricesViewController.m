//
//  SolveMatricesViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SolveMatricesViewController.h"
#import "AppDelegate.h"

@interface SolveMatricesViewController ()

@end

@implementation SolveMatricesViewController
@synthesize x1,x2,x3,y1,y2,y3,z1,z2,z3,activeField;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self moveBannerOffscreen];
     sizesArray = [[NSArray alloc] initWithObjects: @"3 X 3", @"2 X 2", nil];
    _sizePicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 2000, self.view.frame.size.width, 200)];
    [_sizePicker setDataSource: self];
    [_sizePicker setDelegate: self];
    _sizePicker.showsSelectionIndicator = YES;
    
    _rowField.inputView = _sizePicker;
    
    self.adView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait,ADBannerContentSizeIdentifierLandscape,nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
   

    [[_answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[_answerTextView layer] setBorderWidth:2.3];
    [[_answerTextView layer] setCornerRadius:15];
    [_answerTextView setClipsToBounds: YES];
    
    [[_answerWebView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[_answerWebView layer] setBorderWidth:2.3];
    [[_answerWebView layer] setCornerRadius:15];
    [_answerWebView setClipsToBounds: YES];


    // Do any additional setup after loading the view from its nib.
}



- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    
        return sizesArray.count;
        

    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

        
    _rowField.text = [sizesArray objectAtIndex:row];
        
    [activeField resignFirstResponder];
    [self configureMatrix];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    activeField = textField;
    
    return YES;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    return [sizesArray objectAtIndex:row];
    
}

-(void)movePickerViewOffScreen:(UIPickerView*) pickerView{
    
    CGRect frame = pickerView.frame;
    pickerView.frame = CGRectMake(frame.origin.x, 2000,frame.size.width, frame.size.height);
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    
}
-(void)bringPickerViewOnScreen:(UIPickerView*) pickerView{
    
    CGRect frame = pickerView.frame;
    pickerView.frame = CGRectMake(frame.origin.x, self.view.frame.size.height,frame.size.width, frame.size.height);
    
    
    
}

-(void)configureMatrix{
    
    NSArray* aRow1 = [[NSArray alloc] initWithObjects:x1,y1,z1, nil];
    NSArray* aRow2 = [[NSArray alloc] initWithObjects:x2,y2,z2, nil];
    NSArray* aRow3 = [[NSArray alloc] initWithObjects:x3,y3,z3, nil];
    NSArray* acolumn2 = [[NSArray alloc] initWithObjects:y1,y2,y3, nil];
    NSArray* acolumn3 = [[NSArray alloc] initWithObjects:z1,z2,z3, nil];
    if ([_rowField.text isEqual: @"1 X 1"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            
            
            
        } for (int i =0; i < [aRow1 count]; i++) {
            [[aRow2 objectAtIndex:i] setHidden:YES];
            [[aRow3 objectAtIndex:i] setHidden:YES];
            [[acolumn2 objectAtIndex:i] setHidden:YES];
            [[acolumn3 objectAtIndex:i] setHidden:YES];
        }
    }
    if ([_rowField.text isEqual: @"2 X 2"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            
        }
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow3 objectAtIndex:i] setHidden:YES];
            [[acolumn3 objectAtIndex:i] setHidden:YES];
            
        }
        [self.b3 setHidden:YES];
        [self.zLabel setHidden:YES];
        [self.leftBracket setFrame:CGRectMake(self.rowField.frame.origin.x -35 , self.rowField.frame.origin.y + 25, self.leftBracket.frame.size.width, 100)];
        [self.leftBracket setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:75.f]];
        [self.rightBracket setFrame:CGRectMake(self.rowField.frame.origin.x + 5, self.rowField.frame.origin.y + 25, self.rightBracket.frame.size.width, 100)];
        [self.rightBracket setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:75.f]];
        
        
        
    }if ([_rowField.text isEqual: @"3 X 3"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            [[aRow3 objectAtIndex:i] setHidden:NO];
            
            
        }
        [self.zLabel setHidden:NO];
        [self.b3 setHidden:NO];
        [self.leftBracket setFrame:CGRectMake(self.rowField.frame.origin.x - 35, self.rowField.frame.origin.y + 14, self.leftBracket.frame.size.width, 150)];
        [self.leftBracket setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:125]];
        [self.rightBracket setFrame:CGRectMake(self.rowField.frame.origin.x + 5, self.rowField.frame.origin.y +14, self.rightBracket.frame.size.width, 150)];
        [self.rightBracket setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:125.f]];
         
    }
    
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma - mark
#pragma iad Banner


-(void) moveBannerOffscreen
{
    CGRect originalScrollFrame = self.scrollView.frame   ;
    CGFloat newScrollHeight = self.view.frame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
    newScrollFrame.size.height = newScrollHeight;
    
    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = -65;
    
    self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    
}
-(void) moveBannerOnScreen
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        CGRect newBannerFrame = self.adView.frame;
        newBannerFrame.origin.y = 0;
        
        CGRect originalScrollFrame = self.scrollView.frame   ;
        //CGFloat newScrollHeight = self.view.frame.size.height - newBannerFrame.size.height;
        CGRect newScrollFrame = originalScrollFrame;
        //newScrollFrame.size.height = newScrollHeight;
        
        [UIView beginAnimations:@"BannerViewIntro" context:NULL];
        self.scrollView.frame = newScrollFrame;
        self.adView.frame = newBannerFrame;
        [UIView commitAnimations];
    }
    
}

-(void) viewWillAppear:(BOOL)animated{
    //[self moveBannerOffscreen];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        //[scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+120)];
        self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    }
    
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        _adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    }else{
        _adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.adView = nil;
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self moveBannerOffscreen];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self moveBannerOnScreen];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)){
        
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierLandscape;
    }else{
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierPortrait;
    }
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
}

-(BOOL) allowActionToRun{
    return YES;
    
}
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = [self allowActionToRun]; // your application implements this method
    if (!willLeave && shouldExecuteAction)
    {
        // insert code here to suspend any services that might conflict with the advertisement
    }
    return shouldExecuteAction;
}



- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
    lastActiveField = activeField;
    activeField = textField;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    //activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        UIInterfaceOrientation orientation = self.interfaceOrientation;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            CGRect aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
                CGPoint scrollPoint = CGPointMake(0.0,50);//- activeField.frame.origin.y) ;//-kbSize.height);
                [self.scrollView setContentOffset:scrollPoint animated:YES];
            }
            
        }else {
            CGRect aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
                CGPoint scrollPoint = CGPointMake(0.0,135);//- activeField.frame.origin.y) ;//-kbSize.height);
                [self.scrollView setContentOffset:scrollPoint animated:YES];
            }
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    [activeField resignFirstResponder];
}


- (IBAction)clearButton:(id)sender {
}

- (IBAction)calculateButtonPressed:(id)sender {
    double numA11 = [self.x1.text doubleValue];
    double numA12 = [self.y1.text doubleValue];
    double numA13 = [self.z1.text doubleValue];
    double numA21 = [self.x2.text doubleValue];
    double numA22 = [self.y2.text doubleValue];
    double numA23 = [self.z2.text doubleValue];
    double numA31 = [self.x3.text doubleValue];
    double numA32 = [self.y3.text doubleValue];
    double numA33 = [self.z3.text doubleValue];
    
    double b1 = [self.b1.text doubleValue];
    double b2 = [self.b2.text doubleValue];
    double b3 = [self.b3.text doubleValue];
    NSString* htmlString;
    double det = 0;
    NSString* answerString;
    NSArray* inverseA;
    NSArray* b;
    
    if ([_rowField.text isEqual:@"2 X 2"]) {
        det = [self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA21 row2col2:numA22];
        double inv11 = numA22/det;
        double inv12 = -1*numA12/det;
        double inv21 = -1*numA21/det;
        double inv22 = numA11/det;
        
        NSArray* row1 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:inv11],[NSNumber numberWithDouble:inv12], nil];
        NSArray* row2 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:inv21],[NSNumber numberWithDouble:inv22], nil];
        
        inverseA = [[NSArray alloc]initWithObjects:row1,row2, nil];
        b = [[NSArray alloc]initWithObjects:[NSNumber numberWithDouble:b1],[NSNumber numberWithDouble:b2], nil];

        
       
        
    }else if ([_rowField.text isEqual:@"3 X 3"]) {
        //getting cofactor matrix
        double det11 = [self get2By2Determinant:numA22 row1col2:numA23 row2col1:numA32 row2col2:numA33];
        double det12 = -1*[self get2By2Determinant:numA21 row1col2:numA23 row2col1:numA31 row2col2:numA33];
        double det13 = [self get2By2Determinant:numA21 row1col2:numA22 row2col1:numA31 row2col2:numA32];
        
        double det21 = -1*[self get2By2Determinant:numA12 row1col2:numA13 row2col1:numA32 row2col2:numA33];
        double det22 = [self get2By2Determinant:numA11 row1col2:numA13 row2col1:numA31 row2col2:numA33];
        double det23 = -1*[self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA31 row2col2:numA32];
        
        double det31 = [self get2By2Determinant:numA12 row1col2:numA13 row2col1:numA22 row2col2:numA23];
        double det32 = -1*[self get2By2Determinant:numA11 row1col2:numA13 row2col1:numA21 row2col2:numA23];
        double det33 = [self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA21 row2col2:numA22];
        
        
        //get det of 3x3
        det = [self get3By3Determinant:numA11 row1col2:numA12 row1col3:numA13 row2col1:numA21 row2col2:numA22 row2col3:numA23 row3col1:numA31 row3col2:numA32 row3col3:numA33];
        
        //getting adjugate (i.e.transpose of cofactor)
        double adj11 = det11;
        double adj12 = det21;
        double adj13 = det31;
        double adj21 = det12;
        double adj22 = det22;
        double adj23 = det32;
        double adj31 = det13;
        double adj32 = det23;
        double adj33 = det33;
        
        
        
        //get inverse
        double inv11 = adj11/det;
        double inv12 = adj12/det;
        double inv13 = adj13/det;
        double inv21 = adj21/det;
        double inv22 = adj22/det;
        double inv23 = adj23/det;
        double inv31 = adj31/det;
        double inv32 = adj32/det;
        double inv33 = adj33/det;
        
        
        
        NSArray* row1 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:inv11],[NSNumber numberWithDouble:inv12],[NSNumber numberWithDouble:inv13], nil];
        NSArray* row2 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:inv21],[NSNumber numberWithDouble:inv22],[NSNumber numberWithDouble:inv23], nil];
        NSArray* row3 = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:inv31],[NSNumber numberWithDouble:inv32],[NSNumber numberWithDouble:inv33], nil];
        inverseA = [[NSArray alloc]initWithObjects:row1,row2, row3, nil];
        b = [[NSArray alloc]initWithObjects:[NSNumber numberWithDouble:b1],[NSNumber numberWithDouble:b2],[NSNumber numberWithDouble:b3], nil];
        

        
    }
    NSMutableArray* xArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < inverseA.count; i++) {
        NSArray* row = inverseA[i];
        double element = 0;
        for (int j = 0; j < row.count ; j++) {
            double rowVal = [row[j] doubleValue];
            double bVal = [b[j] doubleValue];
            element += rowVal*bVal;
        }
        [xArray addObject:[NSNumber numberWithDouble:element]];
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    
    NSString* x = [formatter stringFromNumber:xArray[0]];
    NSString* y = [formatter stringFromNumber:xArray[1]];
    NSString* z;
    if (xArray.count > 2) {
        z = [formatter stringFromNumber:xArray[2]];
        htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1em' ><mrow > <mtext> For the system </mtext> </mrow><br> <math mathsize='1em' > <mtable> <mtr> <mtd><mn> %g</mn><mi>x</mi> <mo> + </mo><mn> %g</mn><mi>y</mi><mo> + </mo><mn> %g</mn><mi>z</mi><mo> = </mo><mn> %g </mn></mtd>   </mtr>       <mtr> <mtd><mn> %g</mn><mi>x</mi> <mo> + </mo><mn> %g</mn><mi>y</mi><mo> + </mo><mn> %g</mn><mi>z</mi><mo> = </mo><mn> %g </mn></mtd>   </mtr>  <mtr> <mtd><mn> %g</mn><mi>x</mi> <mo> + </mo><mn> %g</mn><mi>y</mi><mo> + </mo><mn> %g</mn><mi>z</mi><mo> = </mo><mn> %g </mn>  </mtd> </mtr>      </mtable>  <mrow> <br> <math mathsize='1em' > <mi> x </mi> <mo> = </mo>  <mn> %@ </mn> <mtext>&nbsp; ,  </mtext>  <mi> y </mi> <mo> = </mo>  <mn> %@ </mn> <mtext> &nbsp; and  &nbsp;</mtext> <mi> z </mi> <mo> = </mo>  <mn> %@ </mn> </mrow> </math> </body></html>", numA11,numA12, numA13, b1, numA21,numA22, numA23, b2,numA31, numA32, numA33, b3, x, y, z];
        
        answerString = [NSString stringWithFormat:@"The solution to your system of equations is x = %@, y = %@, and z = %@", x, y,z];
    }else{
        htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1em' ><mrow > <mtext> For the system </mtext> </mrow><br> <math mathsize='1em' > <mtable> <mtr> <mtd><mn> %g</mn><mi>x</mi> <mo> + </mo><mn> %g</mn><mi>y</mi><mo> = </mo><mn> %g </mn></mtd>   </mtr>       <mtr> <mtd><mn> %g</mn><mi>x</mi> <mo> + </mo><mn> %g</mn><mi>y</mi><mo> = </mo><mn> %g </mn></mtd>   </mtr>        </mtable>  <mrow> <br> <math mathsize='1em' > <mi> x </mi> <mo> = </mo>  <mn> %@ </mn> <mtext> &nbsp; and &nbsp;</mtext>  <mi> y </mi> <mo> = </mo>  <mn> %@ </mn> </mrow> </math> </body></html>", numA11,numA12, b1, numA21,numA22, b2, x, y];
        answerString = [NSString stringWithFormat:@"The solution to your system of equations is x = %@ and y = %@", x, y];
    }
    if(det == 0){
        htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'> <h2>The determinant is zero and this matrix is singular, therefore, this matrix does not have an inverse.</h2></html>"];
        answerString = @"Cannot solve this system because there is not an inverse for the matrix";
        
    }

    
    _answerTextView.text = answerString ;
    [self createContentPages:htmlString];
    
    
    
   }

- (void) createContentPages:(NSString*) htmlString{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSString *contentString;
    
    
    contentString = [NSString
                     stringWithFormat:@"<html><head></head><body style='color:black;'><h2>%@ <br /> <br />%@</body></html>",dateString,htmlString];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [appDelegate.summaryStringsArray addObject:contentString];
        
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [self.mainDetailViewController viewWillAppear:YES];
    }
    
}


-(double)get2By2Determinant:(double)r1c1 row1col2:(double)r1c2  row2col1: (double)r2c1 row2col2: (double)r2c2{
    double det = 0;
    det = r1c1 * r2c2 - r1c2 * r2c1;
    return det;
}

-(double)get3By3Determinant:(double)r1c1 row1col2:(double)r1c2 row1col3:(double)r1c3 row2col1: (double)r2c1 row2col2: (double)r2c2 row2col3:(double)r2c3 row3col1:(double)r3c1 row3col2:(double)r3c2 row3col3:(double)r3c3{
    double det11 = [self get2By2Determinant:r2c2 row1col2:r2c3 row2col1:r3c2 row2col2:r3c3];
    double det12 = [self get2By2Determinant:r2c1 row1col2:r2c3 row2col1:r3c1 row2col2:r3c3];
    double det13 = [self get2By2Determinant:r2c1 row1col2:r2c2 row2col1:r3c1 row2col2:r3c2];
    double det = r1c1*det11 - r1c2*det12 + r1c3*det13;
    return det;
    
}



@end
