//
//  QuadraticFormulaViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import "QuadraticFormulaViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"

@implementation QuadraticFormulaViewController
@synthesize aTextField, bTextField, cTextField, directionsTextView, answerTextView, discriminant, navigationBar, scrollView, bannerIsVisible, adView, detailViewController, summaryViewController, mainDetailViewController ;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);//-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

#pragma - mark
#pragma iad Banner


-(void) moveBannerOffscreen
{
    CGRect originalScrollFrame = self.scrollView.frame   ;
    CGFloat newScrollHeight = self.view.frame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
    newScrollFrame.size.height = newScrollHeight;
    
    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = -75;
    
    self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    
}
-(void) moveBannerOnScreen
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = 0;
    
    CGRect originalScrollFrame = self.scrollView.frame   ;
    //CGFloat newScrollHeight = self.view.frame.size.height - newBannerFrame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
    //newScrollFrame.size.height = newScrollHeight;
    
    [UIView beginAnimations:@"BannerViewIntro" context:NULL];
    self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    [UIView commitAnimations];
    }
}
-(void) viewWillAppear:(BOOL)animated{
   // [self moveBannerOffscreen  ];
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    }else{
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.adView = nil;
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self moveBannerOffscreen];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self moveBannerOnScreen];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierLandscape;
    else
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierPortrait;
}

-(BOOL) allowActionToRun{
    return YES;
    
}
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = [self allowActionToRun]; // your application implements this method
    if (!willLeave && shouldExecuteAction)
    {
        // insert code here to suspend any services that might conflict with the advertisement
    }
    return shouldExecuteAction;
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];

        return NO;
    }
    
    
    return YES;
}



 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
     [super viewDidLoad];

     [self moveBannerOffscreen];
     [self registerForKeyboardNotifications];
     
     self.adView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait,ADBannerContentSizeIdentifierLandscape,nil];

     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

     [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
     scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
     }

     self.aTextField.delegate = self;
     self.bTextField.delegate = self;
     self.cTextField.delegate = self;
     
     
     // For the border and rounded corners
     [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
     [[answerTextView layer] setBorderWidth:2.3];
     [[answerTextView layer] setCornerRadius:15];
     [answerTextView setClipsToBounds: YES];
     //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
     // For the border and rounded corners
     [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
     [[directionsTextView layer] setBorderWidth:2.3];
     [[directionsTextView  layer] setCornerRadius:15];
     [directionsTextView setClipsToBounds: YES];

 }
 
 

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}


-(IBAction) completeSquareButtonPressed: (id)sender{
	
	
    //Unicode: U+221A, UTF-8: E2 88 9A  ̅
    
//Unicode: U+0305, UTF-8: CC 85
//Unicode: U+1D48A (U+D835 U+DC8A), UTF-8: F0 9D 92 8A
    //NSString *i = @"\u2139";
    //NSString *squareRootSymbol = @"\u221A";
	a = [aTextField.text doubleValue];
	b = [bTextField.text doubleValue];
	c = [cTextField.text doubleValue];
    if(a == 0  ){
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"A coefficient is zero!"
                                                           message:@"Please enter in a value other than zero for the A value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
    }  
    else if (b == 0 && c == 0) {
        
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"X = 0!"
                                                           message:@"Please enter in at least an A value and either a B or C value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        
    }else{

    
	//NSLog(@"%lf %lf %lf", a ,b ,c);
    discriminant = b*b - 4*a*c;
    //NSString* stringDiscriminant = [NSString stringWithFormat:@"discriminant = %g",discriminant];
   // NSLog(@"Discriminatn = %g", discriminant);
    if (discriminant < 0){
       /* discriminant = abs(discriminant);
        double RealPart = -b/2*a ;
        double imaginaryCoeff = sqrt(discriminant)/ 2*a;
            
        NSString* xPlusImaginary = [NSString stringWithFormat: @"%g +  %g%@", RealPart, imaginaryCoeff, i];
            NSString* xMinusImaginary  = [NSString stringWithFormat: @"%g - %g%@", RealPart, imaginaryCoeff,i];
            answerTextView.text = [NSString stringWithFormat:@"x = %@ or x = %@", xPlusImaginary,xMinusImaginary];*/
                answerTextView.text = @"No Real Solution";
            }else{
    xPlus = (-b + sqrt(b*b - 4*a*c))/ (2*a);
	xMin = (-b - sqrt(b*b - 4*a*c))/ (2*a);
	answerTextView.text = [NSString stringWithFormat:@"x = %g or x = %g", xPlus, xMin];
	
	}
    [self createContentPages];
    }
}
- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];

    NSString *aString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:a]];
    NSString *bString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:b]];
    NSString *cString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:c]];

    NSString *contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br/>Solutions of  the quadratic equation:  %@x<sup>2</sup> + %@x + %@<br/> %@ </h2></body></html>",dateString, aString, bString, cString,answerTextView.text ];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
[mainDetailViewController viewWillAppear:YES];    }
}



- (void)dealloc {
	/*[aTextField release];
	[bTextField release];
	[cTextField release];
    [super dealloc];*/
}

@end
