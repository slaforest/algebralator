//
//  DecimalsToFractionsViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DecimalsToFractionsViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
@interface DecimalsToFractionsViewController ()

@end

@implementation DecimalsToFractionsViewController

@synthesize directionsTextView, answerTextView, decimalField, scrollView, adView, bannerIsVisible, mainDetailViewController, detailViewController, summaryViewController,navigationBar, overlineLabel, repeatDigitsLabel, repeatDigitsStepper;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    repeatDigitsStepper.value = 0;
    repeatDigitsStepper.enabled = NO;
    repeatDigitsLabel.text = @"0";
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint;
            if (activeField == decimalField ) {
                scrollPoint = CGPointMake(0.0, kbSize.height - 2.3*activeField.frame.origin.y);//-kbSize.height);
                
            }else{
                scrollPoint = CGPointMake(0.0, kbSize.height - 3.05*activeField.frame.origin.y);//-kbSize.height);
            }
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

#pragma - mark
#pragma iad Banner


-(void) moveBannerOffscreen
{
    CGRect originalScrollFrame = self.scrollView.frame   ;
    CGFloat newScrollHeight = self.view.frame.size.height;
    CGRect newScrollFrame = originalScrollFrame;
    newScrollFrame.size.height = newScrollHeight;
    
    CGRect newBannerFrame = self.adView.frame;
    newBannerFrame.origin.y = -75;
    
    self.scrollView.frame = newScrollFrame;
    self.adView.frame = newBannerFrame;
    
}
-(void) moveBannerOnScreen
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        CGRect newBannerFrame = self.adView.frame;
        newBannerFrame.origin.y = 0;
        
        CGRect originalScrollFrame = self.scrollView.frame   ;
        //CGFloat newScrollHeight = self.view.frame.size.height - newBannerFrame.size.height;
        CGRect newScrollFrame = originalScrollFrame;
        //newScrollFrame.size.height = newScrollHeight;
        
        [UIView beginAnimations:@"BannerViewIntro" context:NULL];
        self.scrollView.frame = newScrollFrame;
        self.adView.frame = newBannerFrame;
        [UIView commitAnimations];
    }
}
-(void) viewWillAppear:(BOOL)animated{
    //[self moveBannerOffscreen];
    
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    }else{
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.adView = nil;
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self moveBannerOffscreen];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self moveBannerOnScreen];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierLandscape;
    else
        self.adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierPortrait;
}

-(BOOL) allowActionToRun{
    return YES;
    
}
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = [self allowActionToRun]; // your application implements this method
    if (!willLeave && shouldExecuteAction)
    {
        // insert code here to suspend any services that might conflict with the advertisement
    }
    return shouldExecuteAction;
}


#pragma mark - View lifecycle
# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
	[sender resignFirstResponder];	
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    NSMutableString *overlineString = [[NSMutableString alloc] initWithString:@""];
    BOOL isNowDecimal;
    
    if (isRepeatingDecimal && textField == decimalField) {
        overlineLabel.text = overlineString;
    }

    if ([string isEqualToString:@""]){ 

        return YES;
    }
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]){
        isNowDecimal = YES  ;
        repeatDigitsStepper.enabled = YES;
    return YES;
    }
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]){
        [overlineString appendString:@" "];
        overlineLabel.text = overlineString;
    return YES;
    }
    unichar c = [string characterAtIndex:0];
    dotIndex = [currentString rangeOfString:@"."].location ;
    decimalLength = currentString.length - dotIndex;
    if (decimalLength > 0) {
        repeatDigitsStepper.maximumValue = decimalLength;
    }
    
   
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c]) {
        
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        return NO;
    }
    
        return YES;
}

-(IBAction)stepperChanged:(id)sender{
    int stepVal = repeatDigitsStepper.value;
    repeatDigitsLabel.text = [NSString stringWithFormat:@"%d", stepVal];
    NSString *currentString = decimalField.text;
    int curStrLength = currentString.length;
    int beginOfBar = (curStrLength - stepVal);
    NSRange barRange = NSMakeRange(beginOfBar, stepVal);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:currentString];
    [attString addAttribute:(NSString*)NSUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                      range:barRange];
    decimalField.attributedText = attString;
    

}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    repeatDigitsStepper.enabled = NO;
    [self moveBannerOffscreen];
    [self registerForKeyboardNotifications];
    
    self.adView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait,ADBannerContentSizeIdentifierLandscape,nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    
   
    
    self.decimalField.delegate = self;
   
    
    
    
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]); 
    
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}

-(IBAction)clearButtonPressed:(id)sender{
    
    decimalField.text = @"";
    overlineLabel.text = @"";
    answerTextView.text = @"";
    repeatDigitsLabel.text = @"0";
    
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}
//TODO: need to change repeating decimal algo

-(IBAction)calculateButtonPressed:(id)sender{
    
    rationalNumber = [decimalField.text doubleValue];
    wholeNumberValue = rationalNumber;
    double decimalOnlyValue = (rationalNumber - wholeNumberValue);
    int repeatingDigitsCount = repeatDigitsStepper.value;
    NSString* originalStr = decimalField.text;
    NSRange range = NSMakeRange(originalStr.length - repeatingDigitsCount, repeatingDigitsCount);
    NSString* repeatingDigits = [originalStr substringWithRange:range];
    NSMutableString* repeatStr = [[NSMutableString alloc]initWithString:originalStr];
    [repeatStr appendString:repeatingDigits];
    [repeatStr appendString:repeatingDigits];
    NSLog(@"%@", repeatStr);
    dotIndex = [originalStr rangeOfString:@"."].location;
    decimalLength = originalStr.length - (dotIndex+1);
    int digitCount = decimalLength;
    NSLog(@"%d", digitCount);
    int initialDenominator;
    int initialNumerator;
    int finalNumerator;
    int finalDenominator;
    double firstVal;
    double secondVal;
    double firstMulitplier;
    double secondMultiplier;
    if (repeatingDigitsCount > 0) {
        //change me....
        double firstNum = fabs([repeatStr doubleValue]);
        firstMulitplier = pow(10, digitCount);
        firstVal = firstMulitplier* firstNum;
        NSLog(@" firstval %f", firstVal);
        double secondNum = fabs([repeatStr doubleValue]);
        secondMultiplier = pow(10, digitCount-repeatingDigitsCount);
        secondVal = secondMultiplier* secondNum;
        NSLog(@" secval %f", secondVal);
        NSLog(@" decval %f", rationalNumber);
        initialNumerator = (int)((firstVal - secondVal) + 0.5);
        initialDenominator = (int)firstMulitplier - secondMultiplier;
        if (rationalNumber < 0) {
            initialNumerator = -1*initialNumerator;
        }
        
    }else {
        initialDenominator = pow(10, digitCount);
        initialNumerator = rationalNumber *(pow(10, digitCount));
    }
    double num = decimalOnlyValue * pow(10 , digitCount);
    decimalValue = (int)round(num + .1);
    finalDenominator = initialDenominator;
    finalNumerator = initialNumerator;
    
     
    NSMutableArray *finalNumeratorFactors = [[NSMutableArray alloc] init ];
    NSMutableArray *finalDenominatorFactors = [[NSMutableArray alloc] init ];
    int commonFactor = 0;
    //Simplify the fraction
    for (int i = 1; i <= abs(finalNumerator) ; i++) {
        if ( abs(finalNumerator)  % i == 0) {
            NSNumber *iObject = [NSNumber numberWithInt:i];
            [finalNumeratorFactors addObject:iObject];
        }
    }
    for (int j = 1; j <= abs(finalDenominator); j++) {
        
        if (abs(finalDenominator) % j == 0) {
            NSNumber *jObject = [NSNumber numberWithInt:j];
            [finalDenominatorFactors addObject:jObject];
        }
    }
    
    for (int numeratorIndex = finalNumeratorFactors.count - 1; numeratorIndex > 0; numeratorIndex--) {
        if (commonFactor != 0) {
            break;
        }
        for (int k = 0; k < finalDenominatorFactors.count; k++) {
            
            
            NSNumber*denominatorElement = [finalDenominatorFactors objectAtIndex:k];
            NSNumber *numeratorElement = [finalNumeratorFactors objectAtIndex:numeratorIndex];
            int numeratorElementInt = [numeratorElement intValue];
            int denominatorElementInt = [denominatorElement intValue];
            
            if(denominatorElementInt == numeratorElementInt){
                commonFactor = [numeratorElement intValue];
                finalNumerator = finalNumerator/ commonFactor;
                finalDenominator = finalDenominator / commonFactor;
                
                break;
            }
        }
    }
    if (decimalValue != 0) {
        
    
    //finalNumerator = finalNumerator + wholeNumberValue*finalDenominator;
    answerTextView.text = [NSString stringWithFormat:@"    %d / %d", finalNumerator, finalDenominator];
    }else {
        answerTextView.text = [NSString stringWithFormat:@"    %d ", wholeNumberValue];

    }
       [self createContentPages];
}


- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
        
    NSString *contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br/> The decimal %g written as a fraction is <br/> %@ </h2></body></html>",dateString,rationalNumber, answerTextView.text];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];        
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;
    
}


@end